/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Devuelve el numero real representado por la cadena hexadecinal
// hex en IEEE 754 simple precision 
// in: hex -> string
// out: com -> string array (posicion 0 cadena con la expliacion de la transformacion)
// in: twoExp -> boolean (si true, deja el numero multiplicado por potencia de 2)
function ieeeToReal(hex, com, twoPow) {
    var str = spaces(22) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
    var num = hexStringToInt(hex);
    var cad = toBinaryString(hexStringToBinString(hex), 32);
    str += "<br>0x" + hex + " to binary: " + htmlStringColor(cad.charAt(0), COLORSIGN);
    str += htmlStringColor(cad.substr(1, 8), COLOREXPONENT) + htmlStringColor(cad.substr(9, 23), COLORSIGNIFICAND);
    var s = ((num & 0x80000000) !== 0) ? 1 : 0;
    var c = (num & 0x7F800000) >> 23;
    var m = (num & 0x7FFFFF);
    //console.log(s+" "+c.toString(16)+" "+m);
    switch (c) {
        case 0:
            str += "<br><br> C = 0b" + htmlStringColor("00000000", COLOREXPONENT) + "  " + htmlArrow + "  Special value";
            if (m === 0) { // cero
                str += "<br> M = 0b" + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND) + "  " + htmlArrow + "  Zero";
                str += "<br><br> Number = <b>0</b>";
                num = 0;
            } else {  // desnormalizado
                var e = -126;
                str += "<br> M = 0b" + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + " != 0b00000000000000000000000  " + htmlArrow + "  Denormalized number";
                str += "<br><br>" + "Sign (-1)" + htmlSup(htmlStringColor("S", COLORSIGN)) + ": (-1)" + htmlSup(htmlStringColor(toBinaryString(s), COLORSIGN)) + "  " + htmlArrow + "  " + ((s === 0) ? "+" : "-");
                str += "<br>" + "Exponent: " + e;
                var em = [];
                var mm = binSignificandToInt(toBinaryString(m.toString(2), 23), em);
                str += "<br>" + "Significand (0.M): 0b0." + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + "  " + htmlArrow + "  0b" + mm.toString(2) + " x 2" + htmlSup(em[0]);
                str += "<br><br>" + "Number: (-1)" + htmlSup("S") + " x 0.M x 2" + htmlSup("-126");
                str += " = <br>" + spaces(8) + "(-1)" + htmlSup(htmlStringColor(s, COLORSIGN)) + " x 0b0." + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + " x 2" + htmlSup(e);
                str += " = <br>" + spaces(8) + "(-1)" + htmlSup(s) + " x 0b" + htmlStringColor(toBinaryString(mm.toString(2)), COLORSIGNIFICAND) + " x 2" + htmlSup(em[0]) + " x 2" + htmlSup(e);
                str += " = <br>" + spaces(8) + "(-1)" + htmlSup(s) + " x 0b" + mm.toString(2) + " x 2" + htmlSup(em[0] + e);
                str += " = <br>" + spaces(8) + ((s === 0) ? "+" : "-") + mm + " x 2" + htmlSup(em[0] + e);
                num = (s === 0) ? 1 : -1;
                num *= mm * Math.pow(2, em[0] + e);
                str += " = <br>" + spaces(8) + "<b>" + num + "</b>";
                if (twoPow && Math.abs(em[0] + e) > 2) {
                    num = ((s === 0) ? "+" : "-") + mm + " x 2" + htmlSup(em[0] + e);
                }
                //console.log(str);
            }
            break;
        case 0xFF:
            str += "<br><br> C = 0b" + htmlStringColor("11111111", COLOREXPONENT) + "  " + htmlArrow + "  Special value";
            if (m === 0) { // cero
                str += "<br> M = 0b" + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND) + "  " + htmlArrow + "  +/- infinity";
                str += "<br><br>" + "Sign (-1)" + htmlSup(htmlStringColor("S", COLORSIGN)) + ": (-1)" + htmlSup(htmlStringColor(s, COLORSIGN)) + "  " + htmlArrow + "  " + ((s === 0) ? "+" : "-");
                str += "<br><br> Number = <b>" + ((s === 0) ? "+" : "-") + " infinity</b>";
                num = (s === 0) ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
            } else {
                str += "<br> M = 0b" + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + " != 0b00000000000000000000000  " + htmlArrow + "  NaN";
                str += "<br><br> Number = <b>NaN</b>";
                num = Number.NaN;
            }
            break;
        default:
            var e = c - 127;
            str += "<br><br> 0b00000000 < " + htmlStringColor("C", COLOREXPONENT) + " = " + htmlStringColor(toBinaryString(c.toString(2), 8), COLOREXPONENT) + " < 0b11111111  " + htmlArrow + "  Normalized number";
            str += "<br><br>" + "Sign (-1)" + htmlSup(htmlStringColor("S", COLORSIGN)) + ": (-1)" + htmlSup(htmlStringColor(s, COLORSIGN)) + "  " + htmlArrow + "  " + ((s === 0) ? "+" : "-");
            str += "<br>" + "Exponent (C - 127): 0b" + htmlStringColor(toBinaryString(c.toString(2), 8), COLOREXPONENT) + " - 127  " + htmlArrow + "  " + c + " - 127 = " + e;
            var em = [];
            var mm = binSignificandToInt("1".concat(toBinaryString(m.toString(2), 23)), em);
            str += "<br>" + "Significand (1.M): 0b1." + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + "  " + htmlArrow + "  0b" + mm.toString(2) + " x 2" + htmlSup(em[0]);
            str += "<br><br>" + "Number: (-1)" + htmlSup("S") + " x 1.M x 2" + htmlSup("C-127");
            str += " = <br>" + spaces(8) + "(-1)" + htmlSup(htmlStringColor(s, COLORSIGN)) + " x 0b1." + htmlStringColor(toBinaryString(m.toString(2), 23), COLORSIGNIFICAND) + " x 2" + htmlSup(e);
            str += " = <br>" + spaces(8) + "(-1)" + htmlSup(s) + " x 0b" + mm.toString(2) + " x 2" + htmlSup(em[0]) + " x 2" + htmlSup(e);
            str += " = <br>" + spaces(8) + "(-1)" + htmlSup(s) + " x 0b" + mm.toString(2) + " x 2" + htmlSup(em[0] + e);
            str += " = <br>" + spaces(8) + ((s === 0) ? "+" : "-") + mm + " x 2" + htmlSup(em[0] + e);
            num = (s === 0) ? 1 : -1;
            num *= mm * Math.pow(2, em[0] + e);
            str += " = <br>" + spaces(8) + "<b>" + num + "</b>";
            if (twoPow && Math.abs((em[0] + e)) > 2) {
                num = ((s === 0) ? "+" : "-") + mm + " x 2" + htmlSup(em[0] + e);
            }
    }
    com[0] = str;
    return num;
}

// Devuelve la representacion IEEE 754 del numero real dado (string)
// in: real -> number (numero real)
// out: com -> string array (posicion 0 cadena con la expliacion de la transformacion)
// in: overflow -> si true, lleva overflow a infinito y underflow a 0, si no devuelve ""
function realToIeee(real, com, overflow) {
    var str = "";
    var ieee = "";
    var partes = [];

    if (!isRealString(real, partes)) {
        str = "Not a real number string";
    } else {
        //console.log(partes);
        if (parseInt(partes[1]) === 0 && parseInt(partes[2]) === 0) {
            if (partes[3] === "0") {// cero
                str += "<br> Special value: " + ((partes[0] === 1) ? "+" : "-") + "0";
                str += "<br><br> IEEE 754: sign (S): " + ((partes[0] === 1) ? "+" : "-") + "  " + htmlArrow + "  S = " + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN);
                str += "<br>" + spaces(10) + "exponent (C): 0  " + htmlArrow + "  C = " + htmlStringColor("00000000", COLOREXPONENT);
                str += "<br>" + spaces(10) + "significand (M): 0  " + htmlArrow + "  M = " + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                ieee = isHexadecimalString(((partes[0] === 1) ? "0" : "8") + "0000000", 8);
                str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor("00000000", COLOREXPONENT) + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
            } else if (partes[3] == "1") { // infinity
                str += "<br> Special value: " + ((partes[0] === 1) ? "+" : "-") + "Infinity";
                str += "<br><br> IEEE 754: sign (S): " + ((partes[0] === 1) ? "+" : "-") + "  " + htmlArrow + "  S = " + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN);
                str += "<br>" + spaces(10) + "exponent (C): 255  " + htmlArrow + "  C = " + htmlStringColor("11111111", COLOREXPONENT);
                str += "<br>" + spaces(10) + "significand (M): 0  " + htmlArrow + "  M = " + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                ieee = isHexadecimalString(((partes[0] === 1) ? "7" : "F") + "F800000", 8);
                str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor("11111111", COLOREXPONENT) + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
            } else if (partes[3] == "2") { // NaN
                str += "<br> Special value: " + "NaN";
                str += "<br><br> IEEE 754: sign (S): " + ((partes[0] === 1) ? "+" : "-") + "  " + htmlArrow + "  S = " + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN);
                str += "<br>" + spaces(10) + "exponent (C): 255  " + htmlArrow + "  C = " + htmlStringColor("11111111", COLOREXPONENT);
                str += "<br>" + spaces(10) + "significand (M): " + htmlStringColor("00000000000000000000001", COLORSIGNIFICAND);
                ieee = isHexadecimalString(((partes[0] === 1) ? "7" : "F") + "F800001", 8);
                str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor("11111111", COLOREXPONENT) + htmlStringColor("00000000000000000000001", COLORSIGNIFICAND);
                str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
            }
        } else {
            str += "<br>" + " Number: " + ((partes[0] === 1) ? "+" : "-") + partes[1] + "." + partes[2];
            str += (partes[3] !== "0") ? " x 2" + htmlSup(partes[3]) : "";
            var citb = [];
            intToBin(partes[1], citb, COLORINTPART);
            var int = parseInt(partes[1]);
            var digits = (int === 0) ? 24 : 24 - int.toString(2).length;
            var cftb = [];
            var frac = fracToBin(partes[2], digits, cftb, COLORFRACPART);
            str += "<br>" + " to binary: " + ((partes[0] === 1) ? "+" : "-") + partes[1] + " = 0b" + htmlStringColor(int.toString(2), COLORINTPART);
            str += "<br>" + spaces(12) + "." + partes[2] + " = 0b0." + htmlStringColor(frac, COLORFRACPART);
            str += "<br>" + spaces(11) + ((partes[0] === 1) ? "+" : "-") + partes[1] + "." + partes[2] + " = " + ((partes[0] === 1) ? "+" : "-") + "0b" + htmlStringColor(int.toString(2), COLORINTPART) + "." + htmlStringColor(frac, COLORFRACPART);
            var exp;
            var m;
            if (int > 0) {
                exp = int.toString(2).length - 1;
                m = int.toString(2).substr(1) + frac;
            } else {
                exp = -1 - frac.indexOf("1");
                m = frac.substr(-exp);
            }
            //console.log(m + " " + m.length);
            if (m.length < 23) {
                var i;
                var j = 23 - m.length;
                for (i = 0; i < j; i++) {
                    m = m.concat("0");
                }
            } else if (m.length > 23) {
                m = m.substr(0, 23);
            }
            str += "<br>" + " Normalize: " + ((partes[0] === 1) ? "+" : "-") + partes[1] + "." + partes[2];
            str += (partes[3] !== "0") ? " x 2" + htmlSup(partes[3]) : "";
            str += " = " + ((partes[0] === 1) ? "+" : "-");
            str += "0b" + htmlStringColor(int.toString(2), COLORINTPART) + "." + htmlStringColor(frac, COLORFRACPART);
            str += (partes[3] !== "0") ? " x 2" + htmlSup(partes[3]) : "";
            str += " = " + htmlStringColor(((partes[0] === 1) ? "+" : "-"), COLORSIGN) + "0b1.";
            str += htmlStringColor(m, COLORSIGNIFICAND) + " x 2" + htmlSup(exp);
            str += (partes[3] !== "0") ? " x 2" + htmlSup(partes[3]) : "";
            exp += parseInt(partes[3]);
            str += "<br> " + spaces(11) + "  " + htmlArrow + "  " + htmlStringColor(((partes[0] === 1) ? "+" : "-"), COLORSIGN) + "0b1.";
            str += htmlStringColor(m, COLORSIGNIFICAND) + " x 2" + htmlSup(htmlStringColor(exp, COLOREXPONENT));
            str += "<br><br> IEEE 754: sign (S): " + htmlStringColor(((partes[0] === 1) ? "+" : "-"), COLORSIGN)
            str += "  " + htmlArrow + "  S = " + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN);
            var c = exp + 127;
            if (c > 0 && c < 255) {  // normalizado
                str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor(exp, COLOREXPONENT) + " + 127 = " + c + "  " + htmlArrow + "  C = " + htmlStringColor(toBinaryString(c, 8), COLOREXPONENT);
                str += "<br>" + spaces(10) + "significand (M): " + htmlStringColor(m, COLORSIGNIFICAND);
                str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT);
                str += htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                ieee = ((partes[0] === 1) ? "0" : "1") + toBinaryString(c, 8) + m;
                str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN);
                str += htmlStringColor(toBinaryString(c, 8), COLOREXPONENT) + htmlStringColor(m, COLORSIGNIFICAND);
                str += "  " + htmlArrow + "  <b>0x";
                //console.log(ieee);
                ieee = isHexadecimalString(parseInt(ieee, 2).toString(16), 8);
                str += ieee + "</b>";
                var aux = [];
                str += "<br><br>Represented number: " + ieeeToReal(ieee, aux, false);
                str += "<br><br>To binary detail:<br>" + htmlTwoColumnFormat("Integer part" + citb[0], "Fractional part" + cftb[0], 8);
            } else if (c >= 255) { // infinito
                str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor(exp, COLOREXPONENT) + " + 127 = " + c + "  " + htmlArrow + "  C >= 255  " + htmlArrow + "  <b> Overflow </b>  " + htmlArrow + "  " + ((partes[0] === 1) ? "+" : "-") + " infinity";
                str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor("11111111", COLOREXPONENT);
                str += "<br>" + spaces(10) + "significand (M): " + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                ieee = isHexadecimalString(((partes[0] === 1) ? "7" : "F") + "F800000", 8);
                str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor("11111111", COLOREXPONENT) + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
                str += "<br><br>To binary detail:<br>" + htmlTwoColumnFormat("Integer part" + citb[0], "Fractional part" + cftb[0], 8);
                if (!overflow)
                    ieee = "";
            } else if (c <= 0) {
                if (exp >= -149) { // desnormalizado
                    str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor(exp, COLOREXPONENT) + " + 127 = " + c + "  " + htmlArrow + "  C <= 0, exp = " + exp + " >= -149  " + htmlArrow + "  Denormalized representation";
                    str += "<br> Denormalize: " + htmlStringColor(((partes[0] === 1) ? "+" : "-"), COLORSIGN) + "0b1.";
                    str += htmlStringColor(m, COLORSIGNIFICAND) + " x 2" + htmlSup(htmlStringColor(exp, COLOREXPONENT));
                    var dif = -127 - exp;
                    m = stringExtendLeft("1" + m, "0", dif);
                    str += "  " + htmlArrow + "  " + htmlStringColor(((partes[0] === 1) ? "+" : "-"), COLORSIGN) + "0b0.";
                    str += htmlStringColor(m.substr(0, 23), COLORSIGNIFICAND) + m.substr(23) + " x 2" + htmlSup(htmlStringColor("-126", COLOREXPONENT));
                    m = m.substr(0, 23);
                    c = "00000000";
                    ieee = ((partes[0] === 1) ? "0" : "1") + toBinaryString(c, 8) + m;
                    ieee = isHexadecimalString(parseInt(ieee, 2).toString(16), 8);
                    str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor(toBinaryString(c, 8), COLOREXPONENT);
                    str += "<br>" + spaces(10) + "significand (M): " + htmlStringColor(m, COLORSIGNIFICAND);
                    str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                    str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor(c, COLOREXPONENT) + htmlStringColor(m, COLORSIGNIFICAND);
                    str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
                    var aux = [];
                    str += "<br><br>Represented number: " + ieeeToReal(ieee, aux, false);
                    str += "<br><br>To binary detail:<br>" + htmlTwoColumnFormat("Integer part" + citb[0], "Fractional part" + cftb[0], 8);
                } else { // underflow -> 0
                    str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor(exp, COLOREXPONENT) + " + 127 = " + c + "  " + htmlArrow + "  C < 0, exp = " + exp + " < -149  " + htmlArrow + "  <b> Underflow </b>(" + ((partes[0] === 1) ? "+" : "-") + "0)";
                    str += "<br>" + spaces(10) + "exponent (C): " + htmlStringColor("00000000", COLOREXPONENT);
                    str += "<br>" + spaces(10) + "significand (M): " + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                    ieee = isHexadecimalString(((partes[0] === 1) ? "0" : "8") + "0000000", 8);
                    str += "<br><br>" + spaces(10) + htmlStringColor("S", COLORSIGN) + htmlStringColor("CCCCCCCC", COLOREXPONENT) + htmlStringColor("MMMMMMMMMMMMMMMMMMMMMMM", COLORSIGNIFICAND);
                    str += "<br>" + spaces(10) + htmlStringColor(((partes[0] === 1) ? "0" : "1"), COLORSIGN) + htmlStringColor("00000000", COLOREXPONENT) + htmlStringColor("00000000000000000000000", COLORSIGNIFICAND);
                    str += "  " + htmlArrow + "  <b>0x" + ieee + "</b>";
                    str += "<br><br>To binary detail:<br>" + htmlTwoColumnFormat("Integer part" + citb[0], "Fractional part" + cftb[0], 8);
                    if (!overflow)
                        ieee = "";
                }
            }
        }
    }
    com[0] = str;
    return ieee;
}

// Chequea el resultado del test1 de IEEE (ieee) de Hex a real (number).
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// y en rate[0:1] los aciertos:total preguntas
function ieeeTest1Check(ieee, number, results, rate) {
    ieee = ieee.replace(/\s/g, '').toUpperCase();
    number = number.replace(/\s/g, '').toUpperCase();
    var com = [];
    var aciertos = 0;
    var ans = realToIeee(number, com, false);
    //console.log(number + " : " + ans);
    if (ans === ieee) {
        results[0] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        var n = ieeeToReal(ieee, com, true);
        if (typeof (n) == "number" && isNaN(n)) {
            if (number === "NAN" || number.substr(1) === "NAN") {
                results[0] = htmlStringColor("<b>OK</b>", "green");
                aciertos++;
            } else {
                var cad = toBinaryString(hexStringToBinString(ieee), 32);
                results[0] = htmlStringColor("<b>" + n + "</b>", "red") + " (" + htmlStringColor(cad.charAt(0), COLORSIGN) + htmlStringColor(cad.substr(1, 8), COLOREXPONENT) + htmlStringColor(cad.substr(9, 23), COLORSIGNIFICAND) + ")";
            }
        } else {
            var cad = toBinaryString(hexStringToBinString(ieee), 32);
            results[0] = htmlStringColor("<b>" + n + "</b>", "red") + " (" + htmlStringColor(cad.charAt(0), COLORSIGN) + htmlStringColor(cad.substr(1, 8), COLOREXPONENT) + htmlStringColor(cad.substr(9, 23), COLORSIGNIFICAND) + ")";
        }
    }
    rate[0] = aciertos;
    rate[1] = 1;
}

// Chequea el resultado del test2 de real (number) a hex IEEE (ieee).
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// y rate[0:1] los aciertos:total preguntas
function ieeeTest2Check(ieee, number, results, rate) {
    ieee = delHexFormat(ieee);
    number = number.replace(/\s/g, '').toUpperCase();
    var com = [];
    var aciertos = 0;
    var ans = realToIeee(number, com, false);
    if (ans === ieee) {
        results[0] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        var cad = toBinaryString(hexStringToBinString(ans), 32);
        results[0] = htmlStringColor("<b>" + ans + "</b>", "red") + " (" + htmlStringColor(cad.charAt(0), COLORSIGN) + htmlStringColor(cad.substr(1, 8), COLOREXPONENT) + htmlStringColor(cad.substr(9, 23), COLORSIGNIFICAND) + ")";
    }
    rate[0] = aciertos;
    rate[1] = 1;
}

// real -> string con el numero real
// partes -> [signo (1/-1), parte entera, parte fraccionaria, exponente de 2^ si existe]
// -1.9x2^8  "+htmlArrow+"  partes = [-1, 1, 9, 8]
function isRealString(real, partes) {
    var i;
    var sign = 1;
    var dotpos = -1;
    var powpos = real.length;
    var ini = 0;

    real = real.replace(/\s/g, '').toUpperCase();

    if (real.length === 0)
        return false;
    if (real.charAt(0) === '-') {
        sign = -1;
        ini++;
    } else if (real.charAt(0) === '+') {
        ini++;
    }
    if (real.substr(ini) === "INFINITY") {
        partes[0] = sign;
        partes[1] = "0";
        partes[2] = "0";
        partes[3] = "1";
        return true;
    } else if (real.substr(ini) === "NAN") {
        partes[0] = sign;
        partes[1] = "0";
        partes[2] = "0";
        partes[3] = "2";
        return true;
    }
    for (i = ini; i < real.length; i++) {
        var c = real.charAt(i);
        if (c === '.') {
            if (dotpos === -1 && powpos === real.length)
                dotpos = i;
            else
                return false;
        } else if (c === 'X' || c === '*') {
            if (powpos === real.length && (i + 1) < real.length && real.charAt(i + 1) === '2' &&
                    (i + 2) < real.length && real.charAt(i + 2) === '\^') {
                if (dotpos === -1) {
                    dotpos = i;
                }
                powpos = i;
                i = i + 2;
            } else
                return false;
        } else if (!(c >= '0' && c <= '9') && !((c == "-" || c === "+") && i === powpos + 3)) {
            return false;
        }
    }
    if (dotpos === -1)
        dotpos = real.length;
    partes[0] = sign;
    partes[1] = (ini < dotpos) ? real.substring(ini, dotpos) : "0";
    partes[2] = (dotpos < real.length) ? ((dotpos < powpos - 1) ? real.substring(dotpos + 1, powpos) : "0") : "0";
    partes[3] = (powpos < real.length && powpos + 3 < real.length) ? real.substring(powpos + 3) : "0";
    if (partes[1] === "0" && partes[2] === "0")
        partes[3] = "0";
    //console.log(partes);
    return true;
}

function binSignificandToFloat(bin) {
    var num = 0;
    var aux = 1 / 2;
    var i;
    for (i = 0; i < bin.length; i++) {
        if (bin.charAt(i))
            num += aux;
        aux = aux / 2;
    }
    return num;
}

function binSignificandToInt(bin, exp) {
    var num = 0;
    exp[0] = -23;
    num = parseInt(bin, 2);
    while ((num & 1) === 0) {
        num = num >> 1;
        exp[0]++;
    }
    return num;
}

function fracToBin(frac, digits, com, color) {
    var str = "";
    var cm = "";
    var fr = parseInt(frac);
    var limit = "1";
    /*var i;
     for (i = 0; i < frac.length; i++) {
     limit = limit.concat("0");
     }*/
    limit = stringExtendRight(limit, "0", frac.length);
    limit = parseInt(limit);
    var exit = (digits >= 24) ? false : true;
    var count = 0;
    var signif = 0;
    while (fr !== 0 && (!exit || count < digits)) {
        var aux = fr * 2;
        cm += "<br>0." + (fr + limit).toString().substr(1) + "x2 = ";
        count++;
        if (aux >= limit) {
            str = str.concat("1");
            fr = aux - limit;
            cm += htmlStringColor("<b>1</b>", color) + ".";
            signif++;
        } else {
            str = str.concat("0");
            fr = aux;
            cm += htmlStringColor("<b>0</b>", color) + ".";
            if (signif > 0)
                signif++;
        }
        if (!exit && signif >= 24)
            exit = true;
        cm += (fr + limit).toString().substr(1);
        cm += " (" + count + "th)";
    }
    com[0] = cm;
    return str;
}

function intToBin(int, com, color) {
    var str = "";
    var cm = "";
    var fr = parseInt(int);
    var n = fr.toString(2).length;
    var indent = 0;
    while (fr > 1) {
        var aux = Math.floor(fr / 2);
        var rem = fr % 2;
        if (aux > 1) {
            var auxs = fr + "/2 = " + aux + " rem: " + htmlStringColor("<b>" + rem + "</b>", color);
            var posRem = htmlLength(auxs);
            if (indent === 0) {
                indent = posRem;
            } else {
                auxs = spaces(indent - posRem) + auxs;
            }
            auxs += " (" + (n--) + "th)";
            cm += "<br>" + auxs;
            str = rem.toString() + str;
        } else {
            var auxs = fr + "/2 = " + htmlStringColor("<b>" + aux + "</b>", color) + " rem: " + htmlStringColor("<b>" + rem + "</b>", color);
            var posRem = htmlLength(auxs);
            if (indent === 0) {
                indent = posRem;
            } else {
                auxs = spaces(indent - posRem) + auxs;
            }
            auxs += " (1th, " + (n--) + "th)";
            cm += "<br>" + auxs;
            str = "1" + rem + str;
        }
        fr = aux;
    }
    com[0] = cm;
    return str;
}

function IEEEGetRandomRepresentation() {
    var normal = 20;
    var desnormal = 5;
    var inf = 1;
    var nan = 1;
    var total = normal + desnormal + inf + nan;
    var caso = Math.floor(Math.random() * total);
    //console.log(total + " : " + caso);
    var ieee = "";
    if (caso < normal) {// normal
        var signiBits = 6;
        var m = Math.floor(Math.random() * Math.pow(2, signiBits)).toString(2);
        m = stringExtendRight(m, "0", 23 - m.length);
        var ex = Math.floor(Math.random() * 256).toString(2);
        ex = stringExtendRight(ex, "0", 8 - ex.length);
        ieee += (Math.random() & 1) + ex + m;
        ieee = binStringToHexString(ieee);
    } else if (caso < normal + desnormal) { // desnormalizado
        var signiBits = 6;
        var m = Math.floor(Math.random() * Math.pow(2, signiBits)).toString(2);
        m = stringExtendLeft(m, "0", signiBits - m.length);
        var des = Math.floor(Math.random() * (23 - signiBits));
        m = stringExtendLeft(m, "0", des);
        m = stringExtendRight(m, "0", 23 - des - signiBits);
        ieee += (Math.random() & 1) + "00000000" + m;
        ieee = binStringToHexString(ieee);
    } else if (caso < normal + desnormal + inf) { // infinito
        ieee += (((Math.random() & 1) === 1) ? "F" : "7") + "F800000";
    } else { // NaN
        var signiBits = 6;
        var m = Math.floor(Math.random() * Math.pow(2, signiBits)).toString(2);
        m = stringExtendLeft(m, "0", signiBits - m.length);
        var des = Math.floor(Math.random() * (23 - signiBits));
        m = stringExtendLeft(m, "0", des);
        m = stringExtendRight(m, "0", 23 - des - signiBits);
        ieee += (Math.random() & 1) + "11111111" + m;
        ieee = binStringToHexString(ieee);
    }
    return ieee.toUpperCase();
}

function IEEEGetRandomNumber() {
    var normal = 3;
    var pow2 = 3;
    var desnormal = 1;
    var total = normal + pow2 + desnormal;
    var caso = Math.floor(Math.random() * total);
    var num;
    if (caso < normal) { //normal
        var signiBits = 7;
        num = (Math.floor(Math.random() * Math.pow(2, signiBits)) - Math.pow(2, signiBits - 1)).toString();
    } else if (caso < normal + pow2) { //pow2
        var signiBits = 7;
        var expBits = 5;
        num = (Math.floor(Math.random() * Math.pow(2, signiBits)) - Math.pow(2, signiBits - 1)).toString();
        var exp = (Math.floor(Math.random() * Math.pow(2, expBits)) - Math.pow(2, expBits - 1)).toString();
        num += " x 2\^" + exp;
    } else { //desnormal
        var signiBits = 7;
        var expBits = 5;
        num = (Math.floor(Math.random() * Math.pow(2, signiBits)) - Math.pow(2, signiBits - 1)).toString();
        var exp = (-128 - Math.floor(Math.random() * Math.pow(2, expBits))).toString();
        num += " x 2\^" + exp;
    }
    return num;
}