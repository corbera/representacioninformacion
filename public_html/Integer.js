/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// devuelve el valor natural representado por la cadena
// binaria bin, con la explicacion en com[0]
// out -> numero natural
// in: bin -> cadena binaria (string)
// out: com -> explicacion (array string, en posicion 0)
function binToNatural(bin, com) {
    var str = "";
    var num = 0;
    var len = bin.length;
    var pos = len - 1;
    str += printBinWithPos(bin);
    var i;
    str += " " + htmlArrow + " ";
    var indent = htmlMaxLineLength(str);
    var line1 = "";
    var line2 = "";
    for (i = 0; i < len; i++) {
        num = num << 1;
        num += parseInt(bin.charAt(i));
        if (i !== 0) {
            line1 += " + ";
            if (bin.charAt(i) === "1")
                line2 += " + ";
        }
        if (bin.charAt(i) === "1") {
            line1 += htmlStringColor("<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--), COLORBINONES);
            line2 += Math.pow(2, len - 1 - i);
        } else
            line1 += "<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--);
    }
    num = parseInt(bin, 2);
    //str += fitLineToWindow(indent, line1);
    str += line1;
    line2 += " = <b>" + num + "</b>";
    if (num !== 0)
        //str += " = <br>" + spaces(indent) + fitLineToWindow(indent, line2);
        str += " = <br>" + spaces(indent) + line2;
    else
        str += " = <b>" + num + "</b>";
    str += "<br><br>";
    str += "Number = <b>&#8512;(BIT" + htmlSub("i") + " x 2" + htmlSup("i") + ")</b>, for i = 0 .. MSB";
    com[0] = str;
    return num;
}

// devuelve el valor entero representado por la cadena
// binaria bin en SM, con la explicacion en com[0]
// out -> numero entero
// in: bin -> cadena binaria (string)
// out: com -> explicacion (array string, en posicion 0)
function binToSM(bin, com) {
    var str = "";
    var num = 0;
    var len = bin.length;
    var pos = len - 1;
    str += printSMWithPos(bin);
    var i;
    str += " " + htmlArrow + " ";
    var indent = htmlMaxLineLength(str);
    var sign = parseInt(bin.charAt(0));
    str += "(-1)" + htmlSup("<b>" + htmlStringColor(sign, COLORBINSIGN) + "</b>") + " x (";
    var str2 = "";
    var first = true;
    if (len === 1)
        str += "0";
    for (i = 1; i < len; i++) {
        num = num << 1;
        num += parseInt(bin.charAt(i));
        if (i !== 1) {
            str += " + ";
            if (bin.charAt(i) === "1" && !first)
                str2 += " + ";
        }
        if (bin.charAt(i) === "1") {
            first = false;
            str += htmlStringColor("<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--), COLORBINONES);
            str2 += Math.pow(2, len - 1 - i);
        } else
            str += "<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--);
    }
    if (sign === 1)
        num *= -1;
    str += ") = <br>" + spaces(indent) + ((sign === 0) ? "+1 x " : " -1 x ");
    if (num !== 0)
        str += str2 + " = <b>" + num + "</b>";
    else
        str += "0 = <b>" + ((sign === 1) ? "-" : "+") + num + "</b>";
    str += "<br><br>";
    str += "Number = <b>(-1)" + htmlSup("MSB") + " x (&#8512; BIT" + htmlSub("i") + " x 2" + htmlSup("i") + ")</b>, for i = 0 .. MSB-1";
    com[0] = str;
    com[0] = str;
    return num;
}

// devuelve el valor entero representado por la cadena
// binaria bin en C2, con la explicacion en com[0]
// out -> numero entero
// in: bin -> cadena binaria (string)
// out: com -> explicacion (array string, en posicion 0)
function binToC2(bin, com) {
    var str = "";
    var num = 0;
    var len = bin.length;
    var pos = len - 1;
    str += printSMWithPos(bin);
    var i;
    str += " " + htmlArrow + " ";
    var indent = htmlMaxLineLength(str);
    var str2 = "";
    var c2 = -1 * parseInt(bin.charAt(0)) * Math.pow(2, len - 1);
    for (i = 0; i < len; i++) {
        if (i !== 0) {
            num = num << 1;
            num += parseInt(bin.charAt(i));
        }
        if (i !== 0) {
            str += " + ";
            if (bin.charAt(i) === "1")
                str2 += " + ";
        } else {
            str += " -";
        }
        if (i === 0) {
            str += htmlStringColor("<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--), COLORBINSIGN);
            if (bin.charAt(i) === "1")
                str2 += "-" + Math.pow(2, len - 1 - i);
        } else if (bin.charAt(i) === "1") {
            str += htmlStringColor("<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--), COLORBINONES);
            str2 += Math.pow(2, len - 1 - i);
        } else
            str += "<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--);
    }
    num += c2;
    str += " = <br>" + spaces(indent) + str2 + " = <b>" + num + "</b>";
    if (num < 0) {
        str += "<br><br>Alternative:<br>";
        str += printSMWithPos(bin);
        str += "  " + htmlArrow + "  MSB = " + htmlStringColor("1", COLORBINSIGN) + "  " + htmlArrow + "  Number = -(C2(" + bin + ")) = -(C1(" + bin + ") + 1) = ";
        str += "<br>" + spaces(indent) + "-(0b" + C1(bin) + " + 1) = -(" + (-num - 1) + " + 1) = <b>" + num + "</b>";
    }
    str += "<br><br>";
    str += "Number = <b>-BIT" + htmlSub("MSB") + " x 2" + htmlSup("MSB") + " + (&#8512; BIT" + htmlSub("i") + " x 2" + htmlSup("i") + ")</b>, for i = 0 .. MSB-1";
    com[0] = str;
    return num;
}

// devuelve el valor entero representado por la cadena
// binaria bin en exceso excess, con la explicacion en com[0]
// out -> numero natural
// in: bin -> cadena binaria (string)
// in: escess -> esceso (string)
// out: com -> explicacion (array string, en posicion 0)
function binToExcess(bin, excess, com) {
    var str = "";
    var num = 0;
    var len = bin.length;
    var pos = len - 1;
    str += printBinWithPos(bin);
    var i;
    str += " " + htmlArrow + " (";
    var indent = htmlMaxLineLength(str) - 1;
    var str2 = "";
    for (i = 0; i < len; i++) {
        num = num << 1;
        num += parseInt(bin.charAt(i));
        if (i !== 0) {
            str += " + ";
            if (bin.charAt(i) === "1")
                str2 += " + ";
        }
        if (bin.charAt(i) === "1") {
            str += htmlStringColor("<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--), COLORBINONES);
            str2 += Math.pow(2, len - 1 - i);
        } else
            str += "<b>" + bin.charAt(i) + "</b>" + "x2" + htmlSup(pos--);
    }
    str += ") <b>- " + excess + "</b>";
    if (num !== 0) {
        str += " = <br>" + spaces(indent) + "(" + str2 + ") - " + excess + " = " + "(" + num + ") - " + excess;
        num -= parseInt(excess);
        str += " = <b>" + num + "</b>";
    } else {
        num -= parseInt(excess);
        str += " = <b>" + num + "</b>";
    }
    str += "<br><br>";
    str += "Number = <b>&#8512;(BIT" + htmlSub("i") + " x 2" + htmlSup("i") + ") - M</b>, for i = 0 .. MSB";
    com[0] = str;
    return num;
}

// devuelve el valor natural representado por la cadena
// binaria bin en BCD, con la explicacion en com[0]
// out -> numero natural (-1 si no representa un numero correcto en BCD)
// in: bin -> cadena binaria (string)
// out: com -> explicacion (array string, en posicion 0)
function binToBCD(bin, com) {
    var str = "";
    var num = 0;
    var len = bin.length;
    var ceros = len & 3;
    ceros = (4 - ceros) & 3;
    bin = stringExtendLeft(bin, "0", ceros);
    str += printBinAsBCD(bin);
    str += " " + htmlArrow + " ";
    var indent = htmlMaxLineLength(str);
    var str2 = "";
    var err = "";
    var str3 = "";
    var colors = [COLORBCD1, COLORBCD2];
    var i = 0;
    var pos = len - 1 >> 2;
    var first = true;
    while (i < len) {
        var subcad = bin.substr(i, 4);
        var digit = parseInt(subcad, 2);
        if (!first) {
            str += " + ";
            str2 += " + ";
            str3 += " + ";
        } else {
            first = false;
        }
        str += "(" + htmlStringColor("0b" + subcad, colors[(i >> 2) & 1]) + ") x 10" + htmlSup(pos);
        if (digit < 10) {
            str2 += "(" + digit + ") x " + Math.pow(10, pos);
            str3 += digit * Math.pow(10, pos);
        } else {
            str2 += "(<b>X</b>) x 10" + htmlSup(pos);
            str3 += "X";
            err += "<br> " + htmlStringColor("0b" + subcad, colors[(i >> 2) & 1]) + " (" + digit + ") isn't a valid BCD digit";
        }
        num += digit * Math.pow(10, pos);
        pos--;
        i += 4;
    }
    str += " =<br>" + spaces(indent) + str2 + " =<br> " + spaces(indent) + str3 + " = <b>";
    str += ((err === "") ? num : "X") + "</b>";
    if (err !== "") {
        num = -1;
        str += "<br><br>";
        str += "Binary string doesn't represent a BCD number:";
        str += err;
    }
    com[0] = str + printBCDTable();
    return num;
}

// devuelve el string binario que representa el valor int en binario natural,
// con tantos bits como los indicados en bits, con la explicacion en com[0]
// out -> cadena binaria natural que representa a int
// in: int -> valor natural a representar
// in: bits -> numero de bits
// out: com -> explicacion (array string, en posicion 0)
function naturalToBin(int, bits, com) {
    var str = "";
    var num = parseInt(int);
    if (num < 0) {
        num = "";
        str += "<br> The number <b>" + int + "</b> (< 0) can't be represented in natural binary";
    } else if (num >= Math.pow(2, 32)) {
        num = "";
        str += "<br> The number <b>" + int + "</b> is too big";
    } else {
        var citb = [];
        intToBin(int, citb, COLORINTPART);
        var binstr = num.toString(2);
        str += int + ": to binary  " + htmlArrow + "  0b";
        str += (binstr.length == bits) ? htmlStringColor("<b>" + binstr + "</b>", COLORINTPART) : htmlStringColor(binstr, COLORINTPART);
        if (binstr.length <= bits) {
            str += "<br>0b" + binstr + ": extend to " + bits + " bits  " + htmlArrow + "  0b<b>";
            str += (binstr = stringExtendLeft(binstr, "0", bits - binstr.length)) + "</b>";
            num = binstr;
        } else {
            str += "  " + htmlArrow + "  <b>" + binstr.length + " bits</b> > " + bits + " bits, can't be represented";
            num = "";
        }
        str += "<br><br>To binary detail:" + citb[0];
    }
    com[0] = str;
    return num;
}

// devuelve el string binario que representa el valor int en SM,
// con tantos bits como los indicados en bits, con la explicacion en com[0]
// out -> cadena binaria en SM que representa a int
// in: int -> valor entero a representar
// in: bits -> numero de bits
// out: com -> explicacion (array string, en posicion 0)
function integerToSM(int, bits, com) {
    var str = "";
    var num = parseInt(int);
    var abs = num;
    var sign = 0;
    var signStr = "+";
    if (num < 0) {
        abs = -num;
        sign = 1;
        signStr = "-";
    }
    if (abs >= Math.pow(2, 32)) {
        num = "";
        str += "<br> The number <b>" + int + "</b> is too big";
        com[0] = str;
        return num;
    }
    var citb = [];
    intToBin(abs, citb, COLORINTPART);
    var binstr = abs.toString(2);
    str += int + ": to binary  " + htmlArrow + "  " + htmlStringColor(signStr, COLORBINSIGN) + "0b";
    str += htmlStringColor(binstr, COLORINTPART);
    if (binstr.length < bits - 1) {
        str += "<br> " + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
        str += ": extend to " + (bits - 1) + " bits  " + htmlArrow + "  " + htmlStringColor(signStr, COLORBINSIGN) + "0b";
        str += (binstr = stringExtendLeft(binstr, "0", bits - 1 - binstr.length));
        str += "<br>" + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
        num = sign + binstr;
        str += ": add sign bit  " + htmlArrow + "  0b<b>" + htmlStringColor(sign, COLORBINSIGN) + binstr + "</b>";
    } else if (binstr.length === bits - 1) {
        str += "<br>" + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
        num = sign + binstr;
        str += ": add sign bit  " + htmlArrow + "  0b<b>" + htmlStringColor(sign, COLORBINSIGN) + binstr + "</b>";
    } else {
        str += "  " + htmlArrow + "  <b>" + binstr.length + " bits + 1 sign bit = " + (binstr.length + 1) + "</b> > " + bits + " bits, can't be represented";
        num = "";
    }
    str += "<br><br>To binary detail:" + citb[0];
    com[0] = str;
    return num;
}

// devuelve el string binario que representa el valor int en C2,
// con tantos bits como los indicados en bits, con la explicacion en com[0]
// out -> cadena binaria en C2 que representa a int
// in: int -> valor entero a representar
// in: bits -> numero de bits
// out: com -> explicacion (array string, en posicion 0)
function integerToC2(int, bits, com) {
    var str = "";
    var num = parseInt(int);
    var abs = num;
    var signStr = "+";
    if (num < 0) {
        abs = -num;
        signStr = "-";
    }
    if (abs >= Math.pow(2, 32)) {
        num = "";
        str += "<br> The number <b>" + int + "</b> is too big";
        com[0] = str;
        return num;
    }
    var citb = [];
    intToBin(abs, citb, COLORINTPART);
    var binstr = abs.toString(2);
    str += int + ": to binary  " + htmlArrow + "  " + htmlStringColor(signStr, COLORBINSIGN) + "0b";
    str += htmlStringColor(binstr, COLORINTPART);
    if (binstr.length < bits) {
        str += "<br> " + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
        str += ": extend to " + (bits) + " bits with a 0 as a MSB  " + htmlArrow + "  ";
        if (num >= 0) {
            num = stringExtendLeft(binstr, "0", bits - binstr.length);
            str += "<b>0b" + num + "</b>";
        } else {
            str += htmlStringColor(signStr, COLORBINSIGN) + "0b";
            str += "<b>0</b>" + (stringExtendLeft(binstr, "0", bits - 1 - binstr.length));
            binstr = stringExtendLeft(binstr, "0", bits - binstr.length);
            var str2 = "";
            str2 += "<br>" + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
            str2 += ": add to 2" + htmlSup(binstr.length) + " for C2 representation  " + htmlArrow + "  ";
            var indent = htmlLength(str2);
            str += str2;
            var c2 = stringExtendRight("1", "0", binstr.length);
            str += "0b" + c2;
            str += "<br>" + spaces(indent) + "-0b" + binstr;
            str += "<br>" + spaces(indent + 1) + stringExtendRight("-", "-", binstr.length + 1);
            num = (num & (Math.pow(2, bits) - 1)).toString(2);
            str += "<br>" + spaces(indent + 1) + "<b>0b" + num + "</b>";
            str += "<br><br>Alternative:";
            str += "<br> C2(" + binstr + ") = C1(" + binstr + ") + 1 = 0b" + ((~abs) & (Math.pow(2, bits) - 1)).toString(2) + " + 1 ";
            str += " = <b>0b" + num + "</b>";
        }
    } else if (num !== -Math.pow(2, bits - 1)) {
        str += "  " + htmlArrow + "  <b> MSB is " + binstr.length + "th >= " + bits + "</b>, can't be represented";
        num = "";
    } else {
        str += "<br> " + htmlStringColor(signStr, COLORBINSIGN) + "0b" + binstr;
        str += ": special case: C2(" + binstr + ")";
        str += " = C1(" + binstr + ") + 1 = 0b0" + (abs - 1).toString(2) + " + 1";
        str += " = <b>0b" + binstr + "</b>";
        num = binstr;
    }
    str += "<br><br>To binary detail:" + citb[0];
    com[0] = str;
    return num;
}

// devuelve el string binario que representa el valor int en exceso excess,
// con tantos bits como los indicados en bits, con la explicacion en com[0]
// out -> cadena binaria en exceso que representa a int
// in: int -> valor entero a representar
// in: escess -> exceso
// in: bits -> numero de bits
// out: com -> explicacion (array string, en posicion 0)
function integerToExcess(int, bits, excess, com) {
    var str = "";
    var num = parseInt(int);
    if (num >= Math.pow(2, 32)) {
        num = "";
        str += "<br> The number <b>" + int + "</b> is too big";
        com[0] = str;
        return num;
    }
    num += parseInt(excess);
    str += int + ": add excess " + excess + "  " + htmlArrow + "  " + int + " + " + excess + " = " + num;
    int = num.toString();
    num = naturalToBin(int, bits, com);
    com[0] = str + "<br>" + com[0];
    return num;
}

// devuelve el string binario que representa el valor natural int en BCD,
// con tantos bits como los indicados en bits, con la explicacion en com[0]
// out -> cadena binaria BCD que representa a int
// in: int -> valor natural a representar
// in: bits -> numero de bits
// out: com -> explicacion (array string, en posicion 0)
function naturalToBCD(int, bits, com) {
    var str = "";
    var num = "";
    if (parseInt(int) < 0) {
        str += "The number <b>" + int + "</b> can't be represented in BCD";
    } else {
        var colors = [COLORBCD1, COLORBCD2];
        str += printDecAsBCD(int);
        var indent = htmlLength(str);
        str += "  " + htmlArrow + "  ";
        var i;
        for (i = 0; i < int.length; i++) {
            var digit = int.charAt(i);
            str += htmlStringColor(digit, colors[i & 1]) + "(";
            str += htmlStringColor(BCDdigit(digit), colors[i & 1]) + ")";
        }
        str += "<br>" + spaces(indent) + "  " + htmlArrow + "  ";
        for (i = 0; i < int.length; i++) {
            var digit = parseInt(int.charAt(i));
            str += htmlStringColor(BCDdigit(digit), colors[i & 1]);
            num += BCDdigit(digit);
        }
        if (num.length == bits) {
            str += "  " + htmlArrow + "  <b>0b" + num + "</b>";
        } else if (num.length < bits) {
            num = stringExtendLeft(num, "0", bits - num.length);
            str += ": extend to " + (bits) + " bits  " + htmlArrow + "  <b>0b" + num + "</b>";
        } else {
            str += "  " + htmlArrow + "  <b>needed " + num.length + " bits</b> > " + bits + " bits, can't be represented";
            num = "";
        }
    }
    com[0] = str + printBCDTable();
    return num;
}

// Convierte la cadena binaria bin a todos los formatos de representacion
// de enteros (para el exceso M usa la entrada excess).
function binToAll(bin, excess, com) {
    var str = "";
    var aux = binToNatural(bin, com);
    str += "<br>" + bin + " as a natural binary: <b>" + aux + "</b>";
    aux = binToSM(bin, com);
    str += "<br>" + bin + " as a integer SM: <b>" + aux + "</b>";
    aux = binToC2(bin, com);
    str += "<br>" + bin + " as a integer C2: <b>" + aux + "</b>";
    aux = binToExcess(bin, excess, com);
    str += "<br>" + bin + " as a integer excess " + excess + ": <b>" + aux + "</b>";
    aux = binToBCD(bin, com);
    if (aux >= 0)
        str += "<br>" + bin + " as a natural BCD: <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " doesn't represent any valid BCD number";
    com[0] = str;
}

// Convierte el entero bin a todos los formatos de representacion de
// numeros enteros y naturales con los bits indicados por bits 
// (para el exceso M usa la entrada excess).
function intToAll(bin, bits, excess, com) {
    var str = "";
    var aux = naturalToBin(bin, bits, com);
    if (aux !== "")
        str += "<br>" + bin + " represented as a natural binary: <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " can't be represented as natural binary with " + bits + " bits";
    aux = integerToSM(bin, bits, com);
    if (aux !== "")
        str += "<br>" + bin + " represented as a integer SM: <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " can't be represented as integer in SM with " + bits + " bits";
    aux = integerToC2(bin, bits, com);
    if (aux !== "")
        str += "<br>" + bin + " represented as a integer C2: <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " can't be represented as integer in C2 with " + bits + " bits";
    aux = integerToExcess(bin, bits, excess, com);
    if (aux !== "")
        str += "<br>" + bin + " represented as a integer excess " + excess + ": <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " can't be represented as integer in excess " + excess + " with " + bits + " bits";
    aux = naturalToBCD(bin, bits, com);
    if (aux !== "")
        str += "<br>" + bin + " represented as a natural BCD: <b>" + aux + "</b>";
    else
        str += "<br>" + bin + " can't be represented as natural in BCD with " + bits + " bits";
    com[0] = str;
}

// Chequea los resultados del test 1 donde dada una cadena binString con bitsT1 bits y
// escessVal para exceso M, las respuestas han sido natural, SM, C2, excess y BCD.
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// i-esima, y en rate[0:1] los aciertos:total preguntas
function integerTest1Check(binString, bitsT1, excessVal, natural, SM, C2, excess, BCD, results, rate) {
    var ans;
    var aciertos = 0;
    var total = 0;
    var com = [];

    binString = binString.replace(/\s/g, '').toUpperCase();
    natural = natural.replace(/\s/g, '').toUpperCase();
    SM = SM.replace(/\s/g, '').toUpperCase();
    excess = excess.replace(/\s/g, '').toUpperCase();
    BCD = BCD.replace(/\s/g, '').toUpperCase();

    if ((ans = binToNatural(binString, com)) === parseInt(natural)) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    if ((ans = binToSM(binString, com)) === parseInt(SM)) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    if ((ans = binToC2(binString, com)) === parseInt(C2)) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    if ((ans = binToExcess(binString, excessVal, com)) === parseInt(excess)) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    ans = binToBCD(binString, com);
    ans = (ans < 0) ? "X" : ans;
    if (ans == BCD.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    rate[0] = aciertos;
    rate[1] = total;
}

// Chequea los resultados del test 2 donde dado un numero entero/natural, bitsT2 bits y
// escessVal para exceso M, las respuestas han sido natural, SM, C2, excess y BCD.
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// i-esima, y rate[0:1] los aciertos:total preguntas
function integerTest2Check(number, bitsT2, excessVal, natural, SM, C2, excess, BCD, results, rate) {
    var ans;
    var aciertos = 0;
    var total = 0;
    var com = [];

    natural = delBinaryFormat(natural);
    C2 = delBinaryFormat(C2);
    SM = delBinaryFormat(SM);
    excess = delBinaryFormat(excess);
    BCD = delBinaryFormat(BCD);

    ans = naturalToBin(number, bitsT2, com);
    ans = (ans === "") ? "X" : ans;
    if (ans === natural.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    ans = integerToSM(number, bitsT2, com);
    ans = (ans === "") ? "X" : ans;
    if (ans === SM.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    ans = integerToC2(number, bitsT2, com);
    ans = (ans === "") ? "X" : ans;
    if (ans === C2.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    ans = integerToExcess(number, bitsT2, excessVal, com);
    ans = (ans === "") ? "X" : ans;
    if (ans === excess.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    ans = naturalToBCD(number, bitsT2, com);
    ans = (ans === "") ? "X" : ans;
    if (ans == BCD.toUpperCase()) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    rate[0] = aciertos;
    rate[1] = total;
}

function printBinWithPos(bin) {
    var str = "";
    var len = bin.length;
    if (len < 100) {
        var i;
        var str1 = "", str2 = "";
        for (i = len - 1; i >= 0; i--) {
            if (i > 9)
                str2 += Math.floor(i / 10);
            else
                str2 += "&nbsp;";
            str1 += i % 10;
        }
        if (len > 9) {
            str += spaces(5) + htmlStringColor(str2, COLORBINHEADER) + "<br>";
        }
        str += htmlStringColor("Pos: " + str1, COLORBINHEADER) + "<br>";
        str += spaces(5) + bin;
        return str;
    } else {
        return bin;
    }
}

function printSMWithPos(bin) {
    var str = "";
    var len = bin.length;
    if (len < 100) {
        var i;
        var str1 = "", str2 = "";
        for (i = len - 1; i >= 0; i--) {
            if (i > 9)
                str2 += Math.floor(i / 10);
            else
                str2 += "&nbsp;";
            str1 += i % 10;
        }
        if (len > 9) {
            str += spaces(5) + htmlStringColor(str2, COLORBINHEADER) + "<br>";
        }
        str += htmlStringColor("Pos: " + str1, COLORBINHEADER) + "<br>";
        str += spaces(5) + htmlStringColor("<b>" + bin.charAt(0) + "</b>", COLORBINSIGN) + bin.substr(1);
        return str;
    } else {
        return bin;
    }
}

function printBinAsBCD(bin) {
    var str = "";
    var len = bin.length;
    var ceros = len & 3;
    ceros = (4 - ceros) & 3;
    bin = stringExtendLeft(bin, "0", ceros);
    len = bin.length;
    var i;
    var colors = [COLORBCD1, COLORBCD2];
    for (i = 0; i < len; i++) {
        color = (i >> 2) & 1;
        str += htmlStringColor(bin.charAt(i), colors[color]);
    }
    return str;
}

function C1(bin) {
    var str = "";
    var i;
    for (i = 0; i < bin.length; i++) {
        str += (bin.charAt(i) === "0") ? "1" : "0";
    }
    return str;
}

function BCDdigit(d) {
    var b = d.toString(2);
    return stringExtendLeft(b, "0", 4 - b.length);
}

function printBCDTable() {
    var str = "<br><br>BCD digits<br>----------";
    var i;
    var colors = [COLORBCD1, COLORBCD2]
    for (i = 0; i < 10; i++) {
        str += htmlStringColor("<br>" + i + " = 0b" + BCDdigit(i), colors[i & 1]);
    }
    return str;
}

function printDecAsBCD(int) {
    var str = "";
    var i;
    var colors = [COLORBCD1, COLORBCD2];
    for (i = 0; i < int.length; i++) {
        str += htmlStringColor(int.charAt(i), colors[i & 1]);
    }
    return str;
} 