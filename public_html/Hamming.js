/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Calcula la cadena HammigSEC/DED de datos, devolviendola por 
// hamming y en cabecera el nombre de las posiciones (Pi Di)
// in: datos -> string con 0s y 1s
// out: hamming -> array con un string que es el resultado
// out: comentario -> array con un string con la explicacion de proceso
// in: ded -> boolean, true SEC/DED, false SEC
function toHammingSECDED(datos, hamming, comentario, ded) {
    var d = datos.length;
    var p = 1;
    var com = "";
    while (Math.pow(2, p) < p + d + 1) {
        p++;
    }
    var h = [0];
    var cd = 0;
    var cp = 0;
    var i = 1;
    while (i <= p + d) {
        var tp = twoPower(i);
        if (tp === -1) { // bit de datos
            h[i++] = parseInt(datos.charAt(cd++), 2);
        } else {  // bit de paridad
            h[i++] = 0;
            cp++;
        }
    }
    //console.log(h);
    var bp = 1;
    for (i = 1; i <= cp; i++) {
        //console.log("P" + bp);
        c = [];
        h[Math.pow(2, bp - 1)] = parityAtPos(h, bp - 1, c, true);
        com += "<br> " + htmlStringColor("P" + htmlSub(Math.pow(2, i - 1)), COLORPI) + " =" + c[0] + " = " + htmlStringColor(h[Math.pow(2, bp - 1)], COLORPI);
        //console.log(com);
        bp++;
    }

    cad = "";
    cad2 = [];
    cabecera = [];
    if (ded) {
        c = [];
        h[0] = parity(h, c, true);
        com += "<br> " + htmlStringColor("P", COLORP) + " = " + c[0] + " = " + htmlStringColor(h[0], COLORP);
    }
    //console.log(h);
    var pos = 0;
    for (i = 0; i <= p + d; i++) {
        if (ded && i === 0) {
            cad += h[i];
            cad2[pos] = htmlStringColor(htmlBold(h[i]), COLORP);
            cabecera[pos] = htmlStringColor("P", COLORP);
            pos++;
        } else if (i !== 0) {
            cad += h[i];
            cad2[pos] = (twoPower(i) === -1) ? htmlBold(h[i]) : htmlStringColor(htmlBold(h[i]), COLORPI);
            cabecera[pos] = (twoPower(i) === -1) ? "D" + htmlSub(i) : htmlStringColor("P" + htmlSub(i), COLORPI);
            pos++;
        }
    }
    hamming[0] = cad;
    com += "<br><br>P<sub>i</sub> = XOR of all bits D<sub>j</sub> of input bit string such as";
    com += " j in binary has a 1 in bit number k, with i = 2<sup>k</sup>"
    if (ded) {
        com += "<br>P = XOR of all bits of bit string Hamming SEC";
    }
    comentario[0] = hammingToString(cabecera, cad2, 3) + com;
    return cad;
}

// Chequea si ha habido error y cuantos (y los retorna) y lo corrige si puede
// in: hamming -> string con 0s y 
// out: datos -> array con un string que es el resultado
// out: comentario -> array con un string con la explicacion de proceso
// in: ded -> boolean, true SEC/DED, false SEC
function checkHammingSECDED(hamming, datos, comentario, ded) {
    comentario[0] = "";
    var test = (ded) ? hamming.length - 1 : hamming.length;
    if (test < 3 || twoPower(test) !== -1) {
        comentario[0] += "Bit string ends with a parity bit"
        return "";
    }
    var i;
    var h = (ded) ? [] : [0];
    var pos = (ded) ? 0 : 1;
    var com = "";
    var cont = pos;
    for (i = 0; i < hamming.length; i++)
        h[cont++] = parseInt(hamming.charAt(i), 2);

    //console.log(h);
    var check = [0];
    var len = (ded) ? hamming.length - 1 : hamming.length;
    var cp = Math.ceil(Math.log(len) / Math.log(2));
    for (i = 1; i <= cp; i++) {
        c = [];
        check[i] = parityAtPos(h, i - 1, c, false);
        com += "<br> " + htmlStringColor("C" + htmlSub(Math.pow(2, i - 1)), COLORPI) + " = " + c[0] + " = " + htmlStringColor(check[i], COLORPI);
    }

    if (ded) {
        c = [];
        check[0] = parity(h, c, false);
        com += "<br> " + htmlStringColor("C ", COLORP) + "= " + c[0] + " = " + htmlStringColor(check[0], COLORP);
    }

    //console.log(com);
    var posError = 0;
    var posErrorS = "";
    for (i = 1; i <= cp; i++) {
        posError += check[i] * Math.pow(2, i - 1);
        posErrorS = check[i] + posErrorS;
    }
    //console.log(check);
    //console.log(posError);

    cabecera = [];
    cad = "";
    cad2 = [];

    var pos = 0;
    var len = (ded) ? hamming.length - 1 : hamming.length;
    for (i = 0; i <= len; i++) {
        if (ded && i === 0) {
            cad2[pos] = h[i];
            cabecera[pos] = "P";
            pos++;
        } else if (i !== 0) {
            cad2[pos] = (twoPower(i) !== -1) ? h[i] : htmlStringColor(h[i], COLORDAT);
            cabecera[pos] = (twoPower(i) !== -1) ? "P" + htmlSub(i) : htmlStringColor("D" + htmlSub(i), COLORDAT);
            pos++;
        }
    }
    com = hammingToString(cabecera, cad2, 3) + "<br>" + com;
    var errores = 0;
    if (ded) {
        if (check[0] === 0) {
            if (posError !== 0) {
                com += "<br><br> Double error";
                errores = 2;
            } else {
                com += "<br><br> No error";
            }
        } else if (posError >= h.length) {
            com += "<br><br> Unknown error";
            errores = -1;
        } else {
            com += "<br><br> Single error in position 0b" + htmlStringColor(posErrorS, COLORPI) + " = " + posError + " (" + ((twoPower(posError) === -1) ? "D" : "P") + htmlSub(((posError === 0) ? "" : posError)) + ")";
            com += ": " + h[posError] + " -> " + (h[posError] = (h[posError] + 1) & 1);
            errores = 1;
        }
    } else {
        if (posError === 0) {
            com += "<br><br> No error";
        } else if (posError >= h.length) {
            com += "<br><br> Unknown error";
            errores = -1;
        } else {
            com += "<br><br> Single error in position 0b" + htmlStringColor(posErrorS, COLORPI) + " = " + posError + " (" + ((twoPower(posError) === -1) ? "D" : "P") + htmlSub(((posError === 0) ? "" : posError)) + ")";
            com += ": " + h[posError] + " -> " + (h[posError] = (h[posError] + 1) & 1);
            errores = 1;
        }
    }

    cabecera = [];
    cad = "";
    cad2 = [];
    pos = 0;
    for (i = 1; i < h.length; i++) {
        if (twoPower(i) === -1) {
            cad += (errores !== 0 && errores !== 1) ? "X" : h[i];
            cad2[pos] = (errores !== 0 && errores !== 1) ? htmlBold("X") : ((i === posError) ? htmlStringColor(htmlBold(h[i]), "red") : htmlBold(h[i]));
            cabecera[pos++] = (i === posError) ? htmlStringColor("D" + htmlSub(i), "red") : "D" + htmlSub(i);
        }
    }
    datos[0] = cad;
    comentario[0] = com + "<br><br>" + hammingToString(cabecera, cad2, 3);
    comentario[0] += "<br><br>C<sub>i</sub> = XOR of all bits X<sub>j</sub> (X = P or D) of input bit string such as";
    comentario[0] += " j in binary has a 1 in bit number k, with i = 2<sup>k</sup>"
    if (ded) {
        comentario[0] += "<br>C = XOR of all bits of input bit string";
    }
    comentario[0] += "<br><br> Rules for errors:"
    if (!ded) {
        comentario[0] += "<br>All C<sub>i</sub> == 0  "+htmlArrow+"  no error";
        comentario[0] += "<br>Some C<sub>i</sub> != 0  "+htmlArrow+"  single error at position ";
        comentario[0] += " C<sub>i</sub>C<sub>i-1</sub> ... C<sub>1</sub>C<sub>0</sub>";
        comentario[0] += " (in binary)."
    } else {
        comentario[0] += "<br>C == 0 and all C<sub>i</sub> == 0  "+htmlArrow+"  no error";
        comentario[0] += "<br>C == 0 and some C<sub>i</sub> != 0  "+htmlArrow+"  double error";
        comentario[0] += "<br>C == 1 and all C<sub>i</sub> == 0  "+htmlArrow+"  single error at bit P";
        comentario[0] += "<br>C == 1 and some C<sub>i</sub> != 0  "+htmlArrow+"  single error at position ";
        comentario[0] += " C<sub>i</sub>C<sub>i-1</sub> ... C<sub>1</sub>C<sub>0</sub>";
        comentario[0] += " (in binary)."
    }
    return errores;
    //console.log(cabecera);
    //console.log(datos);
}

// Devuelve la paridad (0,1) de las posiciones tal que dicha posicion en binario
// tiene un 1 en el bit pos
// in: number -> array de enteros a 1 o 0
// in: pos -> entero
// out: comentario -> array con una cadena
// gen: booleano -> true generar bit P, false checkear paridad C
function parityAtPos(number, pos, comentario, gen) {
    var j, count = 0;
    var com = "";
    var ini = true;
    for (j = 1; j <= number.length - 1; j++) {
        if (bitAtPos(j, pos) === 1) {
            //console.log("pos " + j + " = " + number[j]);
            count += number[j];
            if (j === Math.pow(2, pos)) {
                if (!gen) {
                    com += "P" + htmlSub(j) + "(" + number[j] + ")";
                    ini = false;
                }
            } else {
                com += (ini) ? " D" : " xor D";
                com += htmlSub(j) + "(" + number[j] + ")";
                ini = false;
            }
        }
    }
    comentario[0] = com;
    //console.log("count " + count + " &1 " + (count & 1));
    return count & 1;
}

// Devuelve la paridad (0,1) de todas posiciones de number
// in: number -> array de enteros a 1 o 0
// out: comentario -> array con una cadena
// gen: booleano -> true generar bit P, false checkear paridad C
function parity(number, comentario, gen) {
    var j, count = 0;
    var com = "";
    var ini = true;
    for (j = 0; j <= number.length - 1; j++) {
        count += number[j];
        if (twoPower(j) !== -1) {
            if (!gen && j === 0) {
                com += "P" + "(" + number[j] + ")";
                ini = false;
            } else if (j !== 0) {
                com += (ini) ? " P" : " xor P";
                com += htmlSub(j) + "(" + number[j] + ")";
                ini = false;
            }
        } else {
            com += (ini) ? " D" : " xor D";
            com += htmlSub(j) + "(" + number[j] + ")";
            ini = false;
        }
    }
    comentario[0] = com;
    //console.log("count " + count + " &1 " + (count & 1));
    return count & 1;
}

// Devuelve un string formateado con la cabecera y los valores de un Hamming
// in: cabecera -> array de string con los nombres de cada bit
// in: hamming -> string de 0s y 1s con los valores de la cadena hamming
// in: indent -> numero de espacios delante de cada linea
function hammingToString(cabecera, hamming, indent) {
    var i;
    var str = "<table><tr style=\"vertical-align:top\">";
    for (i = 0; i < cabecera.length; i++) {
        str += "<td align=\"center\">" + cabecera[i] + "</td>";
    }
    str += "</tr><trstyle=\"vertical-align:top\">";
    for (i = 0; i < cabecera.length; i++) {
        str += "<td align=\"center\">" + hamming[i] + "</td>";
    }
    str += "</tr></table>";
    return str;
}

// Devuelve un string formateado con la cabecera y los valores de un Hamming SEC
// in: hamming -> string de 0s y 1s con los valores de la cadena hamming
function hammingToStringSec(hamming) {
    var cad2 = [];
    var cabecera = [];
    var pos = 0;
    for (var i = 1; i <= hamming.length; i++) {
        cad2[pos] = htmlBold(hamming.charAt(i-1));
        cabecera[pos] = (twoPower(i) === -1) ? "D" + htmlSub(i) : "P" + htmlSub(i);
        pos++;
    }
    return hammingToString(cabecera, hamming, 0);
}

// Devuelve un string formateado con la cabecera y los valores de un Hamming SEC/DED
// in: hamming -> string de 0s y 1s con los valores de la cadena hamming
function hammingToStringSecDed(hamming) {
    var cad2 = [];
    var cabecera = [];
    var pos = 0;
    for (var i = 0; i < hamming.length; i++) {
        if (i === 0) {
            cad2[pos] = htmlBold(hamming.charAt(i));
            cabecera[pos] = "P";
            pos++;
        } else {
            cad2[pos] = htmlBold(hamming.charAt(i));
            cabecera[pos] = (twoPower(i) === -1) ? "D" + htmlSub(i) : "P" + htmlSub(i);
            pos++;
        }
    }
    return hammingToString(cabecera, hamming, 0);
}

function hammingToStringOld(cabecera, hamming, indent) {
    var i;
    var str = spaces(indent);
    var len = [];
    for (i = 0; i < cabecera.length; i++) {
        str += cabecera[i] + " ";
        len[i] = htmlLength(cabecera[i]);
    }
    str += "<br>" + spaces(indent);
    for (i = 0; i < cabecera.length; i++) {
        str += hamming[i] + spaces(len[i]);
    }
    return str;
}

// Chequea los resultados del test 5 donde dado un string de bits "datos" comprueba
// si las representaciones hammingSec y hammingSecDed dadas por el usuario son
// correctas.
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// i-esima, y en rate[0:1] los aciertos:total preguntas
function hammingTest5Check(datos, hammingSec, hammingSecDed, results, rate) {
    var ans = [];
    var aciertos = 0;
    var total = 0;
    var com = [];

    datos = datos.replace(/\s/g, '').toUpperCase();
    hammingSec = delBinaryFormat(hammingSec);
    hammingSecDed = delBinaryFormat(hammingSecDed);

    toHammingSECDED(datos, ans, com, false);
    if (ans[0] === hammingSec) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;

    toHammingSECDED(datos, ans, com, true);
    if (ans[0] === hammingSecDed) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ans + "</b>", "red");
    }
    total++;
    rate[0] = aciertos;
    rate[1] = total;
}

// Chequea los resultados del test 6 donde dados dos string de bits condificados
// en Haming SEC (hammingSec) y Hamming SEC/DED (hammingSecDed) se pide el dato
// correcto si es posible (dataSec, dataSecDed) (X en caso contrario) y el numero
// de errores que se han producido (errSec, errSecDed).
// un string de bits "datos" comprueba
// En result[i] pondremos la cadena que representa el aciero o fallo en la representacion
// i-esima (del datoSec, datoSecDed, errSec y errSecDed), y en rate[0:1] 
// los aciertos:total preguntas
function hammingTest6Check(hammingSec, hammingSecDed, dataSec, dataSecDed, errSec, errSecDed, results, rate) {
    var aciertos = 0;
    var total = 0;
    var com = [];

    hammingSec = hammingSec.replace(/\s/g, '').toUpperCase();
    hammingSecDed = hammingSecDed.replace(/\s/g, '').toUpperCase();
    dataSec = delBinaryFormat(dataSec);
    dataSecDed = delBinaryFormat(dataSecDed);
    errSec = errSec.replace(/\s/g, '').toUpperCase();
    errSecDed = errSecDed.replace(/\s/g, '').toUpperCase();

    var ansSec = [];
    var ansErrSec = checkHammingSECDED(hammingSec, ansSec, com, false);
    var ansSecDed = [];
    var ansErrSecDed = checkHammingSECDED(hammingSecDed, ansSecDed, com, true);
    if (ansSec[0] === dataSec || (dataSec === "X" && ansSec[0].charAt(0) === "X")) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ansSec[0] + "</b>", "red");
    }
    total++;
    if (ansSecDed[0] === dataSecDed || (dataSecDed === "X" && ansSecDed[0].charAt(0) === "X")) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ansSecDed[0] + "</b>", "red");
    }
    total++;
    if (ansErrSec.toString() === errSec) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ansErrSec + "</b>", "red");
    }
    total++;
    if (ansErrSecDed.toString() === errSecDed) {
        results[total] = htmlStringColor("<b>OK</b>", "green");
        aciertos++;
    } else {
        results[total] = htmlStringColor("<b>" + ansErrSecDed + "</b>", "red");
    }
    total++;
    rate[0] = aciertos;
    rate[1] = total;
}

function hammingGetRandomData(bits) {
    var data = Math.floor(Math.random() * Math.pow(2, bits)).toString(2);
    data = stringExtendLeft(data, "0", bits - data.length);
    return data;
}

function hammingGetRandomBitString(bits, ded) {
    var data = hammingGetRandomData(bits);
    var hamming = [];
    var com = [];
    var nerr = (ded) ? 3 : 2;
    var err = Math.floor(Math.random() * nerr);
    toHammingSECDED(data, hamming, com, ded);
    //console.log(data);
    hamming = hamming[0];
    bits = hamming.length;
    while (err !== 0) {
        err--;
        var biterr = Math.floor(Math.random() * bits);
        var bit = hamming.charAt(biterr);
        bit = (bit === "0") ? "1" : "0";
        var h;
        if (biterr === 0)
            h = bit + hamming.substring(1);
        else if (biterr === bits - 1) {
            h = hamming.substring(0, bits - 1) + bit;
        } else {
            h = hamming.substring(0, biterr) + bit + hamming.substring(biterr + 1);
        }
        hamming = h;
    }
    return hamming;
}

function hammingGetDataHeader(bitsT6, ded, parity) {
    var str = "";
    var pos = 0;
    var cont = 0;
    while (cont < bitsT6) {
        if (twoPower(pos) === -1) {
            str += "D" + htmlSub(pos);
            cont++;
        } else if (parity) {
            if (ded && pos === 0)
                str += "P";
            else if (pos !== 0)
                str += "P" + htmlSub(pos);
        }
        pos++;
    }
    return str;
}

function testHamming() {
    var i;
    for (i = 1; i <= 7; i++) {
        var j;
        for (j = 0; j < Math.pow(2, i); j++) {
            var dat = toBinaryString(j, i);
            var hamming = [];
            var cabecera = [];
            var com = [];
            var dat2 = [];
            var err;
            toHammingSECDED(dat, hamming, cabecera, com, false);
            err = checkHammingSECDED(hamming[0], dat2, cabecera, com, false);
            if (err !== 0 || dat2[0] !== dat) {
                console.log(dat + " -> " + dat2[0]);
            }
            //console.log(dat + " -- " + hamming[0]);
            var k;
            for (k = 0; k < hamming[0].length; k++) {
                var s = hamming[0];
                var c = ((parseInt(s.charAt(k), 2) + 1) & 1).toString();
                s = s.replaceAt(k, c);
                //console.log(hamming[0] + " " + s);
                err = checkHammingSECDED(s, dat2, cabecera, com, false);
                if (err !== 1 || dat2[0] !== dat) {
                    console.log(dat + " -> " + dat2[0]);
                }
            }
            toHammingSECDED(dat, hamming, cabecera, com, true);
            err = checkHammingSECDED(hamming[0], dat2, cabecera, com, true);
            if (err !== 0 || dat2[0] !== dat) {
                console.log(dat + " -> " + dat2[0]);
            }
            for (k = 0; k < hamming[0].length; k++) {
                var s = hamming[0];
                var c = ((parseInt(s.charAt(k), 2) + 1) & 1).toString();
                s = s.replaceAt(k, c);
                //console.log(hamming[0] + " " + s);
                err = checkHammingSECDED(s, dat2, cabecera, com, true);
                if (err !== 1 || dat2[0] !== dat) {
                    console.log(dat + " -> " + dat2[0]);
                }
                var l;
                for (l = 0; l < s.length; l++) {
                    if (l !== k) {
                        var ss = s;
                        c = ((parseInt(ss.charAt(l), 2) + 1) & 1).toString();
                        ss = ss.replaceAt(l, c);
                        //console.log(hamming[0] + " " + s + " " + ss);
                        err = checkHammingSECDED(ss, dat2, cabecera, com, true);
                        if (err !== 2) {
                            console.log(err + ": " + dat + " -> " + dat2[0]);
                        }
                    }
                }
            }
        }
    }
}

