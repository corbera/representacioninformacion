/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const SP = "&nbsp;";

// Devuelve cadena binara si la cadena n representa un numero binario (quitando
// posible 0b inicial
// in: n -> string
function isBinary(n) {
    var i;
    n = delBinaryFormat(n);
    if (n.length > 32)
        return "";
    if (n.length === 0)
        return "";
    for (i = 0; i < n.length; i++) {
        if (n.charAt(i) !== '1' && n.charAt(i) !== '0')
            return "";
    }
    return n;
}

function delBinaryFormat(n) {
    n = n.replace(/\s/g, '').toUpperCase();
    if (n.substring(0, 2) === "0B")
        n = n.substring(2);
    return n;
}

// Si n es potencia de 2 devuelve la potencia, -1 en otro caso
// in: n -> entero
function twoPower(n) {
    var l = Math.log(n) / Math.log(2);
    if (l === Math.floor(l))
        return l;
    else
        return -1;
}

// Devuelve el bit (0,1) de la posición pos de number
// in: number -> entero
// in: pos -> entero
function bitAtPos(number, pos) {  // nubmer Array de enteros 0s 
    return (number >> pos) & 1;
}



String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

function spaces(indent) {
    str = "";
    for (var i = 0; i < indent; i++)
        str += SP;
    return str;
}

function fill(char, num) {
    str = "";
    for (var i = 0; i < num; i++)
        str += char;
    return str;
}

function stringExtend(string, cad, num, dir) {
    var str = string;
    var i;
    for (i = 0; i < num; i++) {
        str = (dir === 0) ? str + cad : cad + str;
    }
    return str;
}

function stringExtendRight(string, cad, num) {
    return stringExtend(string, cad, num, 0);
}

function stringExtendLeft(string, cad, num) {
    return stringExtend(string, cad, num, 1);
}

function toBinaryString(num, digits) {
    var str = num.toString(2);
    if (str.length < digits) {
        /*var i;
         var aux = "";
         for (i = 0; i < digits - str.length; i++) {
         aux += "0";
         }
         str = aux.concat(str);*/
        str = stringExtendLeft(str, "0", digits - str.length);
    }
    return str;
}

function isHexadecimalString(hex, digits) {
    hex = delHexFormat(hex);
    if (digits > 0 && hex.length > digits)
        return "";

    for (var i = 0; i < hex.length; i++) {
        var c = hex.charAt(i);
        if (!(c >= 'A' && c <= 'F') && !(c >= '0' && c <= '9'))
            return "";
    }
    hex = stringExtendLeft(hex, "0", digits - hex.length);
    return hex;
}

function delHexFormat(n) {
    n = n.replace(/\s/g, '').toUpperCase();
    if (n.substring(0, 2) === "0X")
        n = n.substring(2);
    return n;
}

function isDecimalString(dec) {
    if (dec.length === 0)
        return false;
    var i;
    for (i = 0; i < dec.length; i++) {
        var c = dec.charAt(i);
        if (!(c >= '0' && c <= '9'))
            return false;
    }
    return true;
}

function isIntegerDeciamlString(int) {
    if (int.length === 0)
        return false;
    var ini = 0;
    if (int.charAt(0) === "+" || int.charAt(0) === "-")
        ini++;
    return isDecimalString(int.substr(ini));
}

function hexStringToInt(hex) {
    var num = 0;
    var i;
    for (i = 0; i < hex.length; i++) {
        var c = hex.charAt(i);
        var val;
        if (c >= "A" && c <= "F") {
            val = 10 + c.charCodeAt(0) - "A".charCodeAt(0);
        } else {
            val = c.charCodeAt(0) - "0".charCodeAt(0);
        }
        num = num << 4;
        num += val;
    }
    return num;
}

function hexStringToBinString(hex) {
    var num = "";
    var i;
    for (i = 0; i < hex.length; i++) {
        var c = hex.charAt(i);
        var val;
        if (c >= "A" && c <= "F") {
            val = 10 + c.charCodeAt(0) - "A".charCodeAt(0);
        } else {
            val = c.charCodeAt(0) - "0".charCodeAt(0);
        }
        num = num.concat(toBinaryString(val.toString(2), 4));
    }
    return num;
}

function binStringToHexString(bin) {
    var add = 4 - ((bin.length) & 3);
    if (add = 4)
        add = 0;
    var binstr = stringExtendLeft(bin, "0", add);
    var i;
    var str = "";
    for (i = 0; i < binstr.length; i += 4) {
        str += parseInt(binstr.substr(i, 4), 2).toString(16);
    }
    return str;
}

function htmlMaxLineLength(html) {
    var i = 0;
    var maxsize = 0;
    //console.log(col1);
    while (i < html.length) {
        var auxs = html.indexOf("<br>", i);
        if (auxs !== -1) {
            var s = htmlLength(html.substring(i, auxs));
            //console.log(i + " " + auxs + " " + s);
            if (s > maxsize)
                maxsize = s;
            i = auxs + 4;
        } else {
            var s = htmlLength(html.substring(i));
            //console.log(i + " " + auxs + " " + s);
            if (s > maxsize)
                maxsize = s;
            i = html.length;
        }
    }
    return maxsize;
}

function htmlTwoColumnFormat(col1, col2, gap) {
    console.log(col1);
    console.log(col2);
    var maxsize = htmlMaxLineLength(col1);
    maxsize += gap;
    //console.log(maxsize);
    var str = "";
    var i1 = 0;
    var i2 = 0;
    while (i1 < col1.length && i2 < col2.length) {
        var n1 = col1.indexOf("<br>", i1);
        var n2 = col2.indexOf("<br>", i2);
        if (n1 != -1 && n2 != -1) {
            var str1 = col1.substring(i1, n1);
            var str2 = col2.substring(i2, n2);
            str += str1 + spaces(maxsize - htmlLength(str1)) + str2 + "<br>";
            i1 = n1 + 4;
            i2 = n2 + 4;
        } else if (n1 === -1 && n2 === -1) {
            var str1 = col1.substring(i1);
            var str2 = col2.substring(i2);
            str += str1 + spaces(maxsize - htmlLength(str1)) + str2 + "<br>";
            i1 = col1.length;
            i2 = col2.length;
        } else if (n1 === -1) {
            var str1 = col1.substring(i1);
            var str2 = col2.substring(i2, n2);
            str += str1 + spaces(maxsize - htmlLength(str1)) + str2 + "<br>";
            i1 = col1.length;
            i2 = n2 + 4;
        } else {
            var str1 = col1.substring(i1, n1);
            var str2 = col2.substring(i2);
            str += str1 + spaces(maxsize - htmlLength(str1)) + str2 + "<br>";
            i1 = n1 + 4;
            i2 = col2.length;
        }
    }
    if (i1 < col1.length) {
        str += col1.substring(i1);
    } else {
        while (i2 < col2.length) {
            var n2 = col2.indexOf("<br>", i2);
            if (n2 !== -1) {
                str2 = col2.substring(i2, n2);
                str += spaces(maxsize) + str2 + "<br>";
                i2 = n2 + 4;
            } else {
                str2 = col2.substring(i2);
                str += spaces(maxsize) + str2 + "<br>";
                i2 = col2.length;
            }
        }
    }
    return str;
}

function htmlLength(html) {
    var i;
    var len = 0;
    var tag = false;
    var c = " ";
    for (i = 0; i < html.length; i++) {
        var cant = c;
        c = html.charAt(i);
        if (!tag) {
            if ((c === "&") && ((html.substr(i, 6) === SP) ||
                    (html.substr(i, 6) === "&rarr;"))) {
                len++;
                i += 5;
            } else if (c === " " && cant !== " ")
                len++;
            else if (c === "<" && cant !== "\\")
                tag = true;
            else
                len++;
        } else {
            if (c === ">" && cant !== "\\")
                tag = false;
        }
    }
    //console.log(html);
    //console.log(len);
    return len;
}

function htmlStringColor(str, color) {
    return "<font color=\"" + color + "\">" + str + "</font>";
}

function htmlSub(str) {
    return "<sub>" + str + "</sub>";
}

function htmlSup(str) {
    return "<sup>" + str + "</sup>";
}

function htmlBold(str) {
    return "<b>" + str + "</b>";
}

var htmlArrow = "<font size=+2>&rarr;</font>";

function notaTest(aciertos, total) {
    if (total === 0)
        return "";
    var nota = 10 * aciertos / total;
    var color = (nota < 5) ? "red" : "green";
    if (nota == 10)
        return htmlStringColor("<b>" + nota.toFixed(2) + " (" + aciertos + "/" + total + ")</b>", color);
    else
        return htmlStringColor(nota.toFixed(2) + " (" + aciertos + "/" + total + ") (click on errors)", color);
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * 
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
 * 
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}

function htmlFindSpace(html, ini, max) {
    var len = 0;
    var tag = false;
    var c = "";
    var pos = -1;
    for (var i = ini; i < html.length; i++) {
        var cant = c;
        c = html.charAt(i);
        if (!tag) {
            if (html.substr(i, 6) === SP)  {
                pos = i;
                len++;
                i += 5;
            } else if (html.substr(i, 6) === "&rarr;") {
                len++;
                i += 5;
            } else if (c === " " && cant !== " ") {
                len++;
                pos = i;
            } else if (c === "<" && cant !== "\\")
                tag = true;
            else
                len++;
        } else {
            if (c === ">" && cant !== "\\")
                tag = false;
        }
        if (len > max)
            break;
    }
    //console.log(html);
    //console.log(len);
    return pos;
}

/*
 * Devuelve una cadena donde se ha troceado line para que encaje en la ventana
 * actual, deando un espacio inicial (indent) en cada linea (solo para fuente
 * monospace)
 * 
 * @param {int} longitud de la cadena con los espacios en blanco
 * @param {String} cadena a partir
 * 
 * @return {String} cadena con los saltos de linea y la indentacion
 */
function fitLineToWindow(indentChar, line) {
    var scale = 1.2;
    var style = window.getComputedStyle(document.getElementById("resultadoInt1"));
    var fontStyle = style.font;
    var widthPerChar = getTextWidth("a", fontStyle)*scale;
    var indent = spaces(indentChar);
    var widthIndent = getTextWidth(fill("a", indentChar), fontStyle)*scale;
    var maxCharPerLine = (window.innerWidth - widthIndent) / widthPerChar;
    var lineChar = htmlLength(line);
    var pos = 0;
    var str = "";
    var first = true;
    while (htmlLength(line.substring(pos)) > maxCharPerLine) {
        var next = htmlFindSpace(line, pos, maxCharPerLine);
        if (next > 0) {
            if (first) {
                first = false;
            } else {
                str += "<br>" + indent;
            }
            str += line.substring(pos, next);
            pos = next;
        } else {
            break;
        }
    }
    if (!first)
        str += "<br>" + indent;
    str += line.substring(pos);
    return str;
}

function createLineElement(x, y, length, angle) {
    var line = document.createElement("div");
    var styles = 'border: 1px solid black; '
               + 'width: ' + length + 'px; '
               + 'height: 0px; '
               + '-moz-transform: rotate(' + angle + 'rad); '
               + '-webkit-transform: rotate(' + angle + 'rad); '
               + '-o-transform: rotate(' + angle + 'rad); '  
               + '-ms-transform: rotate(' + angle + 'rad); '  
               + 'position: absolute; '
               + 'top: ' + y + 'px; '
               + 'left: ' + x + 'px; ';
    line.setAttribute('style', styles);  
    return line;
}

function createLine(x1, y1, x2, y2) {
    var a = x1 - x2,
        b = y1 - y2,
        c = Math.sqrt(a * a + b * b);

    var sx = (x1 + x2) / 2,
        sy = (y1 + y2) / 2;

    var x = sx - c / 2,
        y = sy;

    var alpha = Math.PI - Math.atan2(-b, a);

    return createLineElement(x, y, c, alpha);
}

var COLORSIGN = "darkorange";
var COLOREXPONENT = "teal";
var COLORSIGNIFICAND = "darkslateblue";
var COLORINTPART = "darkgreen";
var COLORFRACPART = "darkred";

var COLORP = "darkorange";
var COLORPI = "darkred";
var COLORDAT = "teal";

var COLORBINHEADER = "darkred";
var COLORBINONES = "darkblue";
var COLORBINSIGN = "darkgreen";
var COLORBCD1 = "teal";
var COLORBCD2 = "olive";
