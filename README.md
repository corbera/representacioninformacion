# Representacion de la Informacion

Proyecto en Javascript para implementar una herramienta didáctica de ayuda a los alumnos en el tema 
de representación de la información de la asignatura Tecnología de Computadores de los Grados de 
Informática de la Universidad de Málaga.

La herramienta incluye:

* Representación de números enteros: 
	* Binario natural
	* Enteros en signo-magnitud
	* Enteros en complemento a 2
	* Enteros en exceso
	* Naturales en BCD

* Representación de números reales:
	* Representación en punto fijo
	* IEEE 754 simple presición

* Detección y corrección de errores:
	* Codigos Hamming SEC, SEC/DED

Las utilidades permiten la conversión en ambos sentidos (info -> representación, representación -> info),
así como expliación detallada del proceso de conversión de apoyo al alumno. 

